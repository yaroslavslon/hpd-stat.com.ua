(function ($) {
    $(function () {

        $('.button-collapse').sideNav();

    }); // end of document ready
})(jQuery); // end of jQuery name space

function outerHtml(item) {
    return item[0].outerHTML;
}

function selectElementText(el, win) {
    win = win || window;
    var doc = win.document, sel, range;
    if (win.getSelection && doc.createRange) {
        sel = win.getSelection();
        range = doc.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (doc.body.createTextRange) {
        range = doc.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}

function saveJson() {
    var blob = new Blob([$('#div-json').html()], {type: "text/json;charset=utf-8"});
    saveAs(blob, "selection_info.json");
}

function saveXml() {
    var blob = new Blob([$('#div-xml').html()], {type: "text/xml;charset=utf-8"});
    saveAs(blob, "selection_info.xml");
}
function changeForecasting(forecasting) {
    document.getElementById("span-forecasting").innerHTML=forecasting;
}

$(document).ready(function () {// $('.tabs-swipe').tabs({ 'swipeable': true });
    $select = $('select');
    $select.material_select();
    $('.modal').modal();

    $('#json_code_container').hide();
    $('#xml_code_container').hide();

    $('#progress-indeterminate').hide();
    $('#div-region-preloader').hide();

    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 300,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'right', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

    $select.on('contentChanged', function () {
        // re-initialize (update)
        $(this).material_select();
    });

    $("#stat-form").submit(function () {
        if ($('input[name="selection1statAggVal[]"]:checked').length === 0) {
            Materialize.toast('Виберіть тип даних.', 4000, 'red');
            return false;
        }
        return true;
    });

    $selectRegion = $('#select-region');
    $selectRegion.on('change', function () {
        // clear contents
        $preload = $('#div-region-preloader');
        $divRegion = $('#div-region');

        $divRegion.hide();
        $preload.fadeIn('slow', 'swing', function () {
        });


        $selectionSel =
            $('#selection-sel')
                .empty()
                .html(' ');
        $.ajax(
            {
                url: 'getSelections/' + this.value,
                dataType: 'json',
                type: 'get',
                cache: true,
                success: function (json) {
                    $.each(json, function (key, data) {
                        var optionGroupHtml = '';
                        var optionGroupLabel = '';
                        $.each(data, function (index, data) {
                            if (typeof data === 'object')
                                for (var i = 0; i < data.length; ++i)
                                    optionGroupHtml += $("<option></option>")
                                        .attr('value', data[i].t)
                                        .text(data[i].n)[0].outerHTML;
                            else {
                                optionGroupLabel = data;
                            }
                            if (optionGroupHtml !== '') {
                                $selectionSel.append($("<optgroup></optgroup>")
                                    .attr('label', optionGroupLabel)
                                    .html(optionGroupHtml)[0].outerHTML);
                                optionGroupHtml = '';
                                optGroupLabel = '';
                            }
                        });
                    });
                    $select.trigger('contentChanged');

                    $preload.hide();
                    $divRegion.fadeIn('slow', 'swing', function () {
                    });
                    $selectSelection = $('#selection-sel');
                    $selectSelection.val('byRegionAverage');
                    $selectSelection.trigger('change');
                }
            }
        );
    });
    $selectRegion.val('80');
    $selectRegion.trigger('change');

    $('#selection-sel').on('change', function () {

        $progresIindeterminate = $('#progress-indeterminate');
        $progresIindeterminate.fadeIn('slow', 'swing', function () {
        });

        $r = $('#select-region option:selected').attr('value');
        $cur = $('#selection-currency option:selected').attr('value');

        $.ajax(
            {
                url: "getSelectionItems/" + $r + "/" + this.value + "/" + $cur,
                dataType: 'json',
                type: 'get',
                cache: true,
                success: function (json) {
                    $('#json').text('').append(JSON.stringify(json));
                    var containerItems = '';
                    $.each(json.data, function (key, data) {
                        $items = $('<div/>')
                            .addClass('row z-depth-1');
                        if (key !== 0)
                            $items.html($('<h6/>')
                                .addClass('uppercase center-align grey lighten-3')
                                .html('<b>' + key + '</b>'));
                        else
                            $items.html($('<h6/>')
                                .addClass('uppercase center-align grey lighten-3')
                                .html('<b>' + json.selName + '</b>'));

                        if (typeof data === 'object')
                            for (var i = 0; i < data.length; ++i)
                                $items.append($('<div/>').addClass('col s12 m4 l3')
                                    .append($('<input/>')
                                        .attr('id', data[i].v)
                                        .attr('value', data[i].v)
                                        .attr('type', 'radio')
                                        .attr('name', 'selection1statAggVal[]').addClass('filled-in radio-light-blue'))
                                    .append($('<label/>')
                                        .attr('for', data[i].v)
                                        .text(data[i].t)));
                        containerItems += outerHtml($items);

                    });
                    $('#json').html(containerItems);
                }
            }
        );
        $progresIindeterminate.fadeOut('slow', 'swing', function () {
        });
    });
});

