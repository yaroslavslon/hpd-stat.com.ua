$(document).ready(function () {
   $('#app').delay(500).fadeIn('slow');
   $('#loader').delay(500).fadeOut();

   $( ".waves-effect" ).each(function( index ) {
     Waves.attach(this);
 	});
 });
