@extends('layouts.app')


@section('content')

<div class="container">
    <br>
    <h3 class="header center orange-text">Записи із бази даних</h3>
    <div class="row center">
        <h5 class="header col s12 light">Адміністративна частина сайту</h5>
    </div>
    <br>
</div>

<!-- Форма -->
<div class="container">
    <div class="row">
        <table class="striped centered">
            <thead>
            <tr>
                <th>Інформація про вибірку</th>
                <th>Дата останього оновлення</th>
                <th>Популярність</th>
            </tr>
            </thead>

            <tbody>
            @foreach($selections as $selection)
                @php
                        $selectionInfo = json_decode($selection->json_info, true);
                @endphp
                <tr class="hoverable">
                    <td>
                        <div class="col s12">
                                @foreach($selectionInfo as $key => $value)
                                    <p>{{$key}}: <b>{{$value}}</b></p>
                            @endforeach
                            </div>
                        {{--{{ $selection->post_request }}--}}
                    </td>
                    <td>{{ $selection->selection_change_at }}</td>
                    <td>{{ $selection->popular }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="container">
    <div class="row center">
        {!! $selections->links() !!}
    </div>
</div>

@endsection