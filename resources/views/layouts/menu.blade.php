@guest
    <li>
        <a class='dropdown-button' href='#' data-activates='{!! $dropdownServices !!}'>
            <i class="material-icons left">dashboard</i>Сервисы
        </a>
    </li>
    <ul id='{!! $dropdownServices !!}' class='dropdown-content'>
        <li><a href="{{ route('statistic') }}">Статистика цін</a></li>
        <li><a href="{{ route('forecasting') }}">Прогнозування цін</a></li>
    </ul>
    <li>{{Html::linkRoute('register','Реєстрація')}}</li>
    <li>{{Html::linkRoute('login','Вхід')}}</li>
@else
    @if(Auth::user()->role === 'admin')
        <li>
            <a class='dropdown-button' href='#' data-activates='{!! $dropdownAdmin !!}'>
                <i class="material-icons left">settings</i>Админ-панель
            </a>
        </li><!-- Dropdown Structure -->
        <ul id='{!! $dropdownAdmin !!}' class='dropdown-content'>
            <li><a href="{{ route('records') }}">Записи в БД</a></li>
            <li><a href="{{ route('users') }}">Профілі користувачів</a></li>
            <li class="divider"></li>
            <li><a href="#!">Налаштування сайту</a></li>
        </ul>
    @endif
    <li>
        <a class='dropdown-button' href='#' data-activates='{!! $dropdownServices !!}'>
            <i class="material-icons left">dashboard</i>Сервисы
        </a>
    </li>
    <ul id='{!! $dropdownServices !!}' class='dropdown-content'>
        <li><a href="{{ route('statistic') }}">Статистика цін</a></li>
        <li><a href="{{ route('forecasting') }}">Прогнозування цін</a></li>
    </ul>

    <li>
        <a class='dropdown-button' href='#' data-activates='{!! $dropdown !!}'>
        <i class="material-icons left">account_circle</i>{{ Auth::user()->name }}
        </a>
    </li>
    <ul id='{!! $dropdown !!}' class='dropdown-content'>
        <li><a href="#!">Мой профиль</a></li>
        <li class="divider"></li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                Вийти
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@endguest