<script>
    var req = new XMLHttpRequest();
    req.addEventListener("progress", function(event) {
        if (event.lengthComputable) {
            var percentComplete = Math.floor((event.loaded  / event.total) * 100);
            document.getElementById("loader").innerHTML = percentComplete + '%';
        } else {
            document.getElementById("loader").innerHTML = '';
        }
    }, false);

    req.addEventListener("load", function(event) {
        var e = event.target;
        var s = document.createElement("script");
        s.innerHTML = e.responseText;
        document.documentElement.appendChild(s);
        s.addEventListener("load", function() {
        // this runs after the new script has been executed...
    });
    }, false);
    req.open("GET", "{{ mix('js/app.js')}}");
    req.setRequestHeader('Cache-Control', 'public, max-age=31536000');
    req.send();
</script>