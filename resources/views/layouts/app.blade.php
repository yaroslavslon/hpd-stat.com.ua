<!DOCTYPE html>
<html lang="uk">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>@yield('title')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSS  -->
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->

    {{-- <script src="{{ asset('js/head.js') }}"></script> --}}
    
    {{-- <link rel="stylesheet" href="{{ asset('css/highlight-default.min.css')}}">
    <script src="{{ asset('js/highlight.min.js')}}"></script>
    <script>hljs.initHighlightingOnLoad();</script> --}}


    <link href="{{ mix('css/app.css')}}" type="text/css" rel="stylesheet">
    
    <link href="{{ asset('fonts/iconfont/material-icons.css')}}" rel="stylesheet">

</head>

<body>
    @include('layouts.loader')
    <div id="app" style="display: none;">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>

    @include('layouts.script')

</body>

</html>
