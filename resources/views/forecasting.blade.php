@extends('layouts.app')
@section('title', 'Динамика изменения цен на недвижимость')
@section('content')
    <main>
    	<div class="container">
    		<br>
    		<h3 class="header center green-text">Прогнозування змін цін нерухомість</h3>
    		<div class="row center">
    			<h5 class="header col s12 light">Заповніть форму для отримання результатів</h5>
    		</div>
    		<br>
    	</div>
        <v-forecasting></v-forecasting>
    </main>
@endsection
