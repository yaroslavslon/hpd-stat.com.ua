@extends('layouts.app')


@section('content')

    <div class="container">
        <br>
        <h3 class="header center orange-text">Динаміка змін цін на нерухомість в межах України</h3>
        <div class="row center">
            <h5 class="header col s12 light">Заповніть форму для отримання статистичних даних</h5>
        </div>
        <br>
    </div>

    <!-- Форма -->
    <form id="stat-form" action="{{ route('statistic') }}" method="post">
        {{ csrf_field() }}
        @include('sections.input')

        <div id="progress-indeterminate" class="container">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>


        <div class="container">
            <div id="json"></div>
        </div>


        <!-- Кнопка додати вибірку -->
        <div class="container">
            <div class="row center">
                <button type="submit" class="waves-effect waves-light light-blue btn"><i class="material-icons left">add_circle</i>Додати
                    вибірку
                </button>
            </div>
        </div>
    </form>

    @includeWhen(isset($selection) == true, 'sections.info')
    @includeWhen(isset($data) == true, 'sections.data')


@endsection
