@extends('layouts.app')


@section('content')
    <main>

        <div class="container">
            <br>
            <div class="row center">
                <h5 class="header col s12 light">Відновити пароль</h5>
            </div>
            <br>
        </div>

        <form method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">

                    <div class="input-field col s12 m8 offset-m2">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" class="form-control" class="validate" name="email"
                               value="{{ old('email') }}" required autofocus>
                        <label for="email">Email</label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong class="red-text">{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                </div>
            </div>

            <div class="container">
                <div class="row center">
                    <button class="waves-effect waves-light light-blue btn" type="submit"><i
                                class="material-icons left">exit_to_app</i>Відправити посилання на відновлення паролю
                    </button>
                </div>
            </div>

        </form>

    </main>
@endsection
