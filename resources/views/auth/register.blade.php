@extends('layouts.app')

@section('content')

    <div class="container">
        <br>
        <h3 class="header center orange-text">Реєстрація</h3>
        <div class="row center">
            <h5 class="header col s12 light">Заповніть форму реєстраці для відкриття доступу до додаткових функцій</h5>
        </div>
        <br>
    </div>

    <form class="col s12" method="POST" action="{{ route('register') }}">

        <div class="container">
            <div class="row">
                <div class="input-field col s12 m8 offset-m2">
                    @csrf
                    <i class="material-icons prefix">account_circle</i>
                    <input id="login" type="text" class="validate" name="login" value="{{ old('login') }}"
                           required>
                    <label for="login">Логін</label>
                    <span class="helper-text">{{ $errors->first('login') ?? '' }}</span>
                </div>
                <div class="input-field col s12 m8 offset-m2">
                    <i class="material-icons prefix">face</i>
                    <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}"
                           required>
                    <label for="name">Имя</label>
                    <span class="helper-text">{{ $errors->first('name') ?? '' }}</span>
                </div>
                <div class="input-field col s12 m8 offset-m2">
                    <i class="material-icons prefix">email</i>
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}"
                           required>
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="Введений емейл не є коректним" data-success="Емейл є коректним">{{ $errors->first('email') ?? '' }}</span>
                </div>
                <div class="input-field col s12 m8 offset-m2">
                    <i class="material-icons prefix">lock</i>
                    <input id="password" type="password" class="validate" name="password" required>
                    <label for="password">Пароль</label>
                    <span class="helper-text">{{ $errors->first('password') ?? '' }}</span>
                </div>
                <div class="input-field col s12 m8 offset-m2">
                    <i class="material-icons prefix">lock</i>
                    <input id="password_confirmation" type="password" class="validate" name="password_confirmation"
                           required>
                    <label for="password_confirmation">Повторіть пароль</label>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row center">
                <button class="waves-effect waves-light btn" type="submit"><i class="material-icons left">check</i>
                    Завершити реєстрацію
                </button>
            </div>
        </div>

    </form>
@endsection
