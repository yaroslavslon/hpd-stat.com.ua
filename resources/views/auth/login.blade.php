@extends('layouts.app')

@section('content')
<main>
    <div class="container">
        <br>
        <h3 class="header center orange-text">Вхід</h3>
        <div class="row center">
            <h5 class="header col s12 light">Заповніть форму авторизації для входу на сайт</h5>
        </div>
        <br>
    </div>


    <form method="POST" action="{{ route('login') }}">
    <div class="container">
        <div class="row">
                <div class="input-field col s12 m8 offset-m2">
                    @csrf
                    <i class="material-icons prefix">email</i>
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required autofocus>
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="Введений емейл не є коректним" data-success="Емейл є коректним">{{ $errors->first('email') ?? '' }}</span>
                </div>

                <div class="input-field col s12 m8 offset-m2">
                    <i class="material-icons prefix">lock</i>
                    <input id="password" type="password" name="password" class="validate" required>
                    <label for="password">Пароль</label>
                </div>

                <div class="center col s12 m8 offset-m2">
                    <label>
                    <input id="remember-me" type="checkbox"
                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span>Запам'ятати мене</span>
                </label>
                </div>

        </div>
        <div class="row center">
            <button class="waves-effect waves-light btn" type="submit">
                <i class="material-icons left">exit_to_app</i>Увійти
            </button>
        </div>
    </div>

    <div class="container">
        <div class="row center">
            <a href="{{ route('password.request') }}">Забули пароль?</a>
        </div>
    </div>
    </form>
</main>
@endsection
