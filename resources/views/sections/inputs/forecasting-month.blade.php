<!-- Кількість місяців для прогнозування -->
<div class="col s12">
    <p class="black-text center-align">
        Прогнозувати динаміку змін цін на <b><span id="span-forecasting">6</span></b> місяців
    </p>
</div>
<div class="col s12">
    <p class="range-field">
        <input type="range" id="forecasting-range" name="forecastingRange" value="6" min="1" max="12" oninput="changeForecasting(this.value)" onchange="changeForecasting(this.value)"/>
    </p>
</div>