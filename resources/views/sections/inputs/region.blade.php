<!-- Регіон -->
<div class="input-field">
    <select id="select-region" name="statSelectionsRegion">
        <optgroup label="Города">
            <option value="80" selected="selected">Киев</option>
            <option value="85">Севастополь</option>
            <option value="95101">Винница</option>
            <option value="12101">Днепропетровск</option>
            <option value="14101">Донецк</option>
            <option value="18101">Житомир</option>
            <option value="23101">Запорожье</option>
            <option value="26101">Ивано-Франковск</option>
            <option value="35101">Кировоград</option>
            <option value="44101">Луганск</option>
            <option value="97101">Луцк</option>
            <option value="46101">Львов</option>
            <option value="48101">Николаев</option>
            <option value="51101">Одесса</option>
            <option value="53101">Полтава</option>
            <option value="56101">Ровно</option>
            <option value="91101">Симферополь</option>
            <option value="59101">Сумы</option>
            <option value="21101">Ужгород</option>
            <option value="63101">Харьков</option>
            <option value="65101">Херсон</option>
            <option value="68101">Хмельницкий</option>
            <option value="71101">Черкассы</option>
            <option value="74101">Чернигов</option>
            <option value="73101">Черновцы</option>
        </optgroup>
        <optgroup label="Области">
            <option value="91">АР Крым</option>
            <option value="95">Винницкая обл</option>
            <option value="97">Волынская обл</option>
            <option value="12">Днепропетровская обл</option>
            <option value="14">Донецкая обл</option>
            <option value="18">Житомирская обл</option>
            <option value="21">Закарпатская обл</option>
            <option value="23">Запорожская обл</option>
            <option value="26">Ивано-Франковская обл</option>
            <option value="32">Киевская обл</option>
            <option value="35">Кировоградская обл</option>
            <option value="44">Луганская обл</option>
            <option value="46">Львовская обл</option>
            <option value="48">Николаевская обл</option>
            <option value="51">Одесская обл</option>
            <option value="53">Полтавская обл</option>
            <option value="56">Ровенская обл</option>
            <option value="59">Сумская обл</option>
            <option value="61">Тернопольская обл</option>
            <option value="63">Харьковская обл</option>
            <option value="65">Херсонская обл</option>
            <option value="68">Хмельницкая обл</option>
            <option value="71">Черкасская обл</option>
            <option value="74">Черниговская обл</option>
            <option value="73">Черновицкая обл</option>
        </optgroup>
    </select>
    <label>Регіон</label>
</div>