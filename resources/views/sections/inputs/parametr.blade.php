<!-- Параметр -->
<div class="input-field col s12">
    <select id="selection-price" name="selection1priceFld">
        <option value="price_min">Минимальная цена</option>
        <option value="price_max">Максимальная цена</option>
        <option value="price_median">Медианная цена</option>
        <option value="price_avg_clear">Средняя цена</option>
        <option value="pricem_min">Минимальная цена за м.кв.</option>
        <option value="pricem_max">Максимальная цена за м.кв.</option>
        <option value="pricem_median">Медианная цена за м.кв.</option>
        <option value="pricem_avg_clear" selected="">Средняя цена за м.кв.</option>
    </select>
    <label>Параметр</label>
</div>