<table class="striped centered">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Ціна - {{ $selection->subselection_name }}</th>
    </tr>
    </thead>
    <tbody>

    @if (isset($forecastingData) === true)
        @foreach($forecastingData as $date => $price)
            <tr class="hoverable">
                <td><b>{{ $date }}</b></td>
                <td><span class="green-text text-darken-1"><b>{{ $price }}</b></span></td>
            </tr>
        @endforeach
    @endif
    @foreach($selectionData as $date => $price)
        <tr class="hoverable">
            <td> {{ $date }}</td>
            <td> {{ $price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>