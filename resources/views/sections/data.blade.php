<!-- Вибірки -->
<div class="container">
    <div class="section">
        <div class="row">
            <div id="selection1" class="col s12">
                <!-- Tabs (table and plot) -->
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4"><a href="#table1">Таблиця</a></li>
                        <li class="tab col s4"><a href="#plot1">Графік</a></li>
                        <li class="tab col s4"><a href="#save">Зберегти у файл</a></li>
                    </ul>
                </div>
                <div id="table1" class="col s12">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6"><a href="#table-by-date">По датам</a></li>
                            <li class="tab col s6"><a href="#table-by-mounth">По месяцам</a></li>
                        </ul>
                    </div>
                    <div id="table-by-date">
                        @include('sections.table', ['selectionData' => $data['byDate']])
                    </div>
                    <div id="table-by-mounth">
                        @include('sections.table',['selectionData' => $data['byMounth'], 'forecastingData' => $forecast ?? null])
                    </div>
                </div>

                <div id="plot1" class="col s12 centered">
                    <div id="stockChart" style="height: 500px"></div>

                    <script>
                        Highcharts.setOptions({
                            lang: {
                                loading: 'Завантаження...',
                                months: ['Сіяень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
                                weekdays: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятница', 'Субота'],
                                shortMonths: ['Січ', 'Лют', 'Бер', 'Квіт', 'Трав', 'Черв', 'Лип', 'Серп', 'Вер', 'Жовт', 'Лист', 'Груд'],
                                exportButtonTitle: "Експорт",
                                printButtonTitle: "Друк",
                                rangeSelectorFrom: "З",
                                rangeSelectorTo: "По",
                                rangeSelectorZoom: "Період",
                                downloadPNG: 'Завантажити PNG',
                                downloadJPEG: 'Завантажити JPEG',
                                downloadPDF: 'Завантажити PDF',
                                downloadSVG: 'Завантажити SVG',
                                printChart: 'Надрукувати графік'
                            }
                        });

                        // Create the chart
                        Highcharts.stockChart('stockChart', {

                            legend: {
                                enabled: true,
                                verticalAlign: 'bottom'
                            },
                            rangeSelector: {
                                selected: 5,
                                allButtonsEnabled: true,
                                buttons: [{
                                    type: 'month',
                                    count: 1,
                                    text: '1 міс.'
                                }, {
                                    type: 'month',
                                    count: 3,
                                    text: '3 міс.'
                                }, {
                                    type: 'month',
                                    count: 6,
                                    text: '6 міс.'
                                }, {
                                    type: 'ytd',
                                    text: 'С НР'
                                }, {
                                    type: 'year',
                                    count: 1,
                                    text: '1 р.'
                                }, {
                                    type: 'year',
                                    count: 3,
                                    text: '3 р.'
                                }, {
                                    type: 'year',
                                    count: 5,
                                    text: '5 р.'
                                }, {
                                    type: 'all',
                                    text: 'Все'
                                }]
                            },
                            series:
                                [
                                    {
                                        name: '{{ $selection->subselection_name }}',
                                        data: {{ $data['chartByDate'] }},
                                        tooltip:
                                            {
                                                valueDecimals: 2
                                            }
                                    },
                                    {
                                        name: 'По месяцам',
                                        data: {{ $data['chartByMounth'] }},
                                        tooltip:
                                            {
                                                valueDecimals: 2
                                            }
                                    }
                                    @if (isset($chartForecast) === true)
                                    ,
                                    {
                                        name: 'Прогноз',
                                        data: {{ $chartForecast }},
                                        tooltip:
                                            {
                                                valueDecimals: 2
                                            }
                                    }
                                    @endif
                                ]
                        });
                    </script>
                </div>


                <div id="save" class="col s12">

                    <div class="container-rotate">
                        <div class="row center">
                            <br>
                            @guest
                                <div class="col s12 center">
                                    <button class="waves-effect waves-light light-blue btn row"
                                            onclick="$('#json_code_container').show();$('#xml_code_container').hide(); return false;">
                                        Json
                                    </button>
                                </div>
                                <div class="container">
                                    <div class="row center">
                                        <div class="col s12">
                                            Зареєстровані користувачі мають можливість збурігати дані у форматі
                                            <i>xml</i>.
                                        </div>
                                        <div class="col s12">
                                            <a href="{{ route('login') }}">Увійти</a> | <a
                                                    href="{{ route('register') }}">Зареєструватися</a>
                                        </div>
                                    </div>
                                </div>
                            @else

                                <div class="col s12 m6 center">
                                    <button class="waves-effect waves-light light-blue btn row"
                                            onclick="$('#json_code_container').show();$('#xml_code_container').hide(); return false;">
                                        Json
                                    </button>
                                </div>
                                <div class="col s12 m6 center">
                                    <button class="waves-effect waves-light light-blue btn row"
                                            onclick="$('#json_code_container').hide();$('#xml_code_container').show(); return false;">
                                        Xml
                                    </button>
                                </div>
                            @endguest

                            <div id="json_code_container">
                                <div class="col s12 right-align">
                                    <a href="#"
                                       onclick="selectElementText(document.getElementById('code_json'));return false;">Виділити
                                        код</a>
                                    <a href="#" onclick="saveJson();return false;">Скачать</a>
                                </div>
                                <div class="col s12 left-align">
                                    <pre><code id="code_json" class="json">{{ $data['json'] }}</code></pre>
                                    <div id="div-json" class="hide">{{ $data['json'] }}</div>
                                </div>

                            </div>
                            @guest
                            @else
                                <div id="xml_code_container">
                                    <div class="col s12 right-align">
                                        <a href="#"
                                           onclick="selectElementText(document.getElementById('code_xml'));return false;">Виділити
                                            код</a>
                                        <a href="#" onclick="saveXml();return false;">Скачать</a>
                                    </div>
                                    <div class="col s12 left-align">
                                        <pre><code id="code_xml" class="xml">{{ $data['xml'] }}</code></pre>
                                        <div id="div-xml" class="hide">{!! $data['xml'] !!}</div>
                                    </div>
                                </div>
                            @endguest

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
