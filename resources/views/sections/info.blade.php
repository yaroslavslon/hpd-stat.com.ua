{{--About selection--}}
<div class="container">
    <div class="row">
        <ul class="collapsible" data-collapsible="accordion">
            <li>
                <div class="collapsible-header"><i class="material-icons">info</i>Інформація про поточну вибірку
                @if (isset($monthsOfForecasting))
                (Прогноз на {{ $monthsOfForecasting }} міс.)
                @endif
                </div>
                <div class="collapsible-body">
                    <div class="row center">
                        <div class="col s12">
                            <p>Регион: <b>{{ $selection->region_name }}</b></p>
                            <p>Вибирка: <b>{{ $selection->selection_name }}</b></p>
                            <p>Валюта: <b>{{ $selection->currency_name }}</b></p>
                            <p>Апроксимация: <b>{{ $selection->approx_name }}</b></p>
                            <p>Параметр: <b>{{ $selection->parameter_name }}</b></p>
                            <p>Подвиборка: <b>{{ $selection->subselection_name }}</b></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>