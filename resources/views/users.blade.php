@extends('layouts.app')


@section('content')

    <div class="container">
        <br>
        <h3 class="header center orange-text">Зареєстровані користувачі</h3>
        <div class="row center">
            <h5 class="header col s12 light">Адміністративна частина сайту</h5>
        </div>
        <br>
    </div>

    <!-- Форма -->
    <div class="container">
        <div class="row">
            <table class="striped centered">
                <thead>
                <tr>
                    <th>Логін</th>
                    <th>email</th>
                    <th>Права користувача</th>
                    <th>Дата реєстрації</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr class="hoverable">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role }}</td>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Кнопка додати вибірку -->
    <div class="container">
        <div class="row center">
            {!! $users->links() !!}
        </div>
    </div>
@endsection