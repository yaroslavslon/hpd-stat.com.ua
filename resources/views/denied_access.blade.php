@extends('layouts.app')


@section('content')

    <div class="container">
        <br>
        <h3 class="header center orange-text">Відмовлено в доступі</h3>
        <div class="row center">
            <h5 class="header col s12 light">У Вас немає прав переглядати цю сторінку</h5>
        </div>
        <br>
    </div>

@endsection