let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.version();
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/head.js', 'public/js');

let scripts = [
	'public/js/app.js',
	// Materialize.css
    'node_modules/materialize-css/js/cash.js',
    'node_modules/materialize-css/js/component.js',
    'node_modules/materialize-css/js/global.js',
    'node_modules/materialize-css/js/anime.min.js',
    'node_modules/materialize-css/js/collapsible.js',
    'node_modules/materialize-css/js/dropdown.js',
    //'node_modules/materialize-css/js/modal.js',
    //'node_modules/materialize-css/js/materialbox.js',
    //'node_modules/materialize-css/js/parallax.js',
    //'node_modules/materialize-css/js/tabs.js',
    //'node_modules/materialize-css/js/tooltip.js',
    'node_modules/materialize-css/js/waves.js',
    //'node_modules/materialize-css/js/toasts.js',
    'node_modules/materialize-css/js/sidenav.js',
    //'node_modules/materialize-css/js/scrollspy.js',
    //'node_modules/materialize-css/js/autocomplete.js',
    'node_modules/materialize-css/js/forms.js',
    //'node_modules/materialize-css/js/slider.js',
    //'node_modules/materialize-css/js/cards.js',
    //'node_modules/materialize-css/js/chips.js',
    //'node_modules/materialize-css/js/pushpin.js',
    'node_modules/materialize-css/js/buttons.js',
    //'node_modules/materialize-css/js/datepicker.js',
    //'node_modules/materialize-css/js/timepicker.js',
    //'node_modules/materialize-css/js/characterCounter.js',
    //'node_modules/materialize-css/js/carousel.js',
    //'node_modules/materialize-css/js/tapTarget.js',
    'node_modules/materialize-css/js/select.js',
    //'node_modules/materialize-css/js/range.js',

    //'node_modules/file-saver/FileSaver.js',
    //'node_modules/highcharts/themes/dark-green.js'
]

if (mix.inProduction()) {
	mix.babel(scripts, 'public/js/app.js');
}
else {
	mix.combine(scripts, 'public/js/app.js');
}
