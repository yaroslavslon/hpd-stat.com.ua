<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('selections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region_name')->nullable();
            $table->string('selection_name')->nullable();
            $table->string('currency_name')->nullable();
            $table->string('approx_name')->nullable();
            $table->string('subselection_name')->nullable();
            $table->string('parameter_name')->nullable();
            $table->text('post_request');
            $table->text('json_data');
            $table->integer('popular')->default(0);
            $table->dateTime('selection_change_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('selections');
    }
}
