<?php

use HpdStatApp\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $user = new User();

        $user->login = 'PrOsToY_4eL';
        $user->name = 'Yaroslav';
        $user->email = 'yaroslavslon@gmail.com';
        $user->password = Hash::make('12345678');

        $user->save();

        $user = new User();

        $user->login = 'admin';
        $user->name = 'Administrator';
        $user->email = 'admin@gmail.com';
        $user->password = Hash::make('12345678');
        $user->role = 'admin';

        $user->save();
    }
}
