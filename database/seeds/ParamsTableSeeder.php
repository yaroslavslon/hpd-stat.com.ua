<?php

use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class ParamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('params')->insert(
            [
                // Міста
                ['value' => '80', 'name' => 'Київ'],
                ['value' => '85', 'name' => 'Севастополь'],
                ['value' => '95101', 'name' => 'Вінниця'],
                ['value' => '12101', 'name' => 'Дніпро'],
                ['value' => '14101', 'name' => 'Донецьк'],
                ['value' => '18101', 'name' => 'Житомир'],
                ['value' => '23101', 'name' => 'Запоріжжя'],
                ['value' => '26101', 'name' => 'Івано-Франківськ'],
                ['value' => '35101', 'name' => 'Кропивницький'],
                ['value' => '44101', 'name' => 'Луганськ'],
                ['value' => '97101', 'name' => 'Луцьк'],
                ['value' => '46101', 'name' => 'Львів'],
                ['value' => '48101', 'name' => 'Миколаів'],
                ['value' => '51101', 'name' => 'Одеса'],
                ['value' => '53101', 'name' => 'Полтава'],
                ['value' => '56101', 'name' => 'Рівне'],
                ['value' => '91101', 'name' => 'Сімферополь'],
                ['value' => '59101', 'name' => 'Суми'],
                ['value' => '21101', 'name' => 'Ужгород'],
                ['value' => '63101', 'name' => 'Харків'],
                ['value' => '65101', 'name' => 'Херсон'],
                ['value' => '68101', 'name' => 'Хмельницький'],
                ['value' => '71101', 'name' => 'Черкаси'],
                ['value' => '74101', 'name' => 'Чернігів'],
                ['value' => '73101', 'name' => 'Чернівці'],

                //області
                ['value' => '91', 'name' => 'АР Крим'],
                ['value' => '95', 'name' => 'Віницька обл'],
                ['value' => '97', 'name' => 'Волинська обл'],
                ['value' => '12', 'name' => 'Дніпропетровська обл'],
                ['value' => '14', 'name' => 'Донецька обл'],
                ['value' => '18', 'name' => 'Житомирська обл'],
                ['value' => '21', 'name' => 'Закарпатська обл'],
                ['value' => '23', 'name' => 'Запорізька обл'],
                ['value' => '26', 'name' => 'Івано-Франківська обл'],
                ['value' => '32', 'name' => 'Київська обл'],
                ['value' => '35', 'name' => 'Кіровоградська обл'],
                ['value' => '44', 'name' => 'Луганська обл'],
                ['value' => '46', 'name' => 'Львівська обл'],
                ['value' => '48', 'name' => 'Миколаївська обл'],
                ['value' => '51', 'name' => 'Одеська обл'],
                ['value' => '53', 'name' => 'Полтавска обл'],
                ['value' => '56', 'name' => 'Рівненська обл'],
                ['value' => '59', 'name' => 'Сумська обл'],
                ['value' => '61', 'name' => 'Тернопільська обл'],
                ['value' => '63', 'name' => 'Харківська обл'],
                ['value' => '65', 'name' => 'Херсонська обл'],
                ['value' => '68', 'name' => 'Хмельницька обл'],
                ['value' => '71', 'name' => 'Черкаська обл'],
                ['value' => '74', 'name' => 'Чернігівська обл'],
                ['value' => '73', 'name' => 'Чернівецька обл'],

                //Параметр
                ['value' => 'price_min', 'name' => 'Мінімальна ціна'],
                ['value' => 'price_max', 'name' => 'Максимальна ціна'],
                ['value' => 'price_median', 'name' => 'Медіанна ціна'],
                ['value' => 'price_avg_clear', 'name' => 'Середня ціна'],
                ['value' => 'pricem_min', 'name' => 'Мінімальна ціна за м.кв.'],
                ['value' => 'pricem_max', 'name' => 'Максимальна ціна за м.кв.'],
                ['value' => 'pricem_median', 'name' => 'Медіанна ціна за м.кв.'],
                ['value' => 'pricem_avg_clear', 'name' => 'Середня ціна за м.кв.'],

                //Валюта
                ['value' => '1', 'name' => 'Гривня'],
                ['value' => '2', 'name' => 'Долар'],

                //Апроксимація

                ['value' => '', 'name' => 'Немає'],
                ['value' => 'avg3', 'name' => 'Рухоме середне (3 точки)'],
                ['value' => 'avg5', 'name' => 'Рухоме середне (5 точки)'],

                //Виборки
                ['value' => 'Общие данные', 'name' => 'Загальні данні'],

                ['value' => 'byRegionAverage', 'name' => 'За регіоном'],
                ['value' => 'byRayon', 'name' => 'За районом'],
                ['value' => 'byMassiv', 'name' => 'За масивом'],
                ['value' => 'byClass', 'name' => 'За типом об\'єкта'],
                ['value' => 'byHouseType', 'name' => 'За типом будинку'],
                ['value' => 'byWalls', 'name' => 'За типом стін'],
                ['value' => 'byNewbuild', 'name' => 'За новобудовах регіону'],
                ['value' => 'byMitro', 'name' => 'За регіоном - біля метро'],
                ['value' => 'byMitroClass', 'name' => 'За типом об\'єкта - біля метро'],
                ['value' => 'byNewbuildClass', 'name' => 'За типом об\'єкта - новобудови'],

                ['value' => 'По районам', 'name' => 'За районом'],

                ['value' => 'byRayonClass', 'name' => 'За типом об\'єкта'],
                ['value' => 'byRayonHouseType', 'name' => 'За типом будинку'],
                ['value' => 'byRayonWalls', 'name' => 'За типом стін'],
                ['value' => 'byRayonMitro', 'name' => 'Біля метро'],
                ['value' => 'byRayonNewbuild', 'name' => 'Новобудови'],
                ['value' => 'byRayonClassMitro', 'name' => 'За типом об\'єкта - біля метро'],
                ['value' => 'byRayonClassNewbuild', 'name' => 'За типом об\'єкта - новобудови'],
                ['value' => 'byRayonHouseTypeClass', 'name' => 'За типом будинку та типом об\'єкта'],
                ['value' => 'byRayonWallsClass', 'name' => 'За типом стін та типом об\'єкта'],
                ['value' => 'byRayonWallsNewbuild', 'name' => 'За типом стін - новобудови'],
                ['value' => 'byRayonWallsClassNewbuild', 'name' => 'За типами стін та типами об\'єктів - новобудови'],

                ['value' => 'По массивам', 'name' => 'За масивами'],

                ['value' => 'byMassivClass', 'name' => 'За типом об\'єкта'],
                ['value' => 'byMassivHouseType', 'name' => 'За типом будинку'],
                ['value' => 'byMassivWalls', 'name' => 'За типом стін'],
                ['value' => 'byMassivMitro', 'name' => 'Біля метро'],
                ['value' => 'byMassivNewbuild', 'name' => 'Новобудови'],
                ['value' => 'byMassivClassMitro', 'name' => 'За типом об\'єкта - біля метро'],
                ['value' => 'byMassivClassNewbuild', 'name' => 'За типом об\'єкта - новобудови'],
                ['value' => 'byMassivHouseTypeClass', 'name' => 'За типом будинку та типом об\'єкта'],
                ['value' => 'byMassivWallsClass', 'name' => 'За типом стін та типом об\'єкта'],

                ['value' => 'По типам стен', 'name' => 'За типами стін'],

                ['value' => 'byWallsClass', 'name' => 'За типом об\'єкта'],
                ['value' => 'byWallsNewbuild', 'name' => 'Новобудови'],
                ['value' => 'byWallsMitro', 'name' => 'Біля метро'],
                ['value' => 'byWallsClassNewbuild', 'name' => 'За типом об\'єкта - новобудови'],
                ['value' => 'byWallsClassMitro', 'name' => 'За типом об\'єкта - біля метро'],

                ['value' => 'По типам домов', 'name' => 'За типами будинків'],

                ['value' => 'byHouseTypeClass', 'name' => 'За типом об\'єкта'],
                ['value' => 'byHouseTypeMitro', 'name' => 'Біля метро'],
                ['value' => 'byHouseTypeClassMitro', 'name' => 'За типом об\'єкта - біля метро'],
            ]
        );
    }
}
