<?php

namespace HpdStatApp;

use Illuminate\Database\Eloquent\Model;

/**
 * HpdStatApp\SelectionItem.
 *
 * @mixin \Eloquent
 *
 * @property int    $id
 * @property string $request
 * @property mixed  $json
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\SelectionItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\SelectionItem whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\SelectionItem whereRequest($value)
 */
class SelectionItem extends Model
{
    public $timestamps = false;
}
