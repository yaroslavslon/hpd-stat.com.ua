<?php

namespace HpdStatApp;

use Illuminate\Database\Eloquent\Model;

/**
 * HpdStatApp\Selection.
 *
 * @mixin \Eloquent
 *
 * @property int                 $id
 * @property string|null         $region_name
 * @property string|null         $selection_name
 * @property string|null         $currency_name
 * @property string|null         $approx_name
 * @property string|null         $subselection_name
 * @property string|null         $parameter_name
 * @property string              $post_request
 * @property string              $json_data
 * @property int                 $popular
 * @property \Carbon\Carbon|null $selection_change_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereApproxName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereCurrencyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereJsonData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereParameterName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection wherePopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection wherePostRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereRegionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereSelectionChangeAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereSelectionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereSubselectionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Selection whereUpdatedAt($value)
 */
class Selection extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'selection_change_at',
    ];
}
