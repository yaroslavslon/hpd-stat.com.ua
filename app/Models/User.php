<?php

namespace HpdStatApp;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * HpdStatApp\User.
 *
 * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $email
 * @property string              $password
 * @property string              $role
 * @property string|null         $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
