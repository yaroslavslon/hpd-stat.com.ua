<?php

namespace HpdStatApp;

use Illuminate\Database\Eloquent\Model;

/**
 * HpdStatApp\Param.
 *
 * @mixin \Eloquent
 *
 * @property int    $id
 * @property string $value
 * @property string $name
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Param whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Param whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\HpdStatApp\Param whereValue($value)
 */
class Param extends Model
{
}
