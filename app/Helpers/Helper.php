<?php

namespace HpdStatApp\Helpers;

use SoapBox\Formatter\Formatter;

class Helper
{
    public static function setLocale($locale)
    {
        setlocale(LC_TIME, $locale);
    }

    public static function utfDecode($str)
    {
        $string = $str;
        $string = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $string);

        return $string;
    }

    public static function xmlFromJson($json)
    {
        $formatter = Formatter::make($json, Formatter::JSON);
        $xml = $formatter->toXml();
        $domxml = new \DOMDocument('1.0');
        $domxml->preserveWhiteSpace = false;
        $domxml->formatOutput = true;
        $domxml->loadXML($xml);

        return $domxml->saveXML();
    }
}
