<?php

namespace HpdStatApp\Helpers;

use GuzzleHttp\Client;
use HpdStatApp\Param;
use HpdStatApp\SelectionItem;

use Illuminate\Support\Facades\Response;

class SelectionItemHelper
{
    public static function getSelections($request)
    {
        $item = SelectionItem::where('request', $request)->first();
        if (!$item) {
            $client = new Client();
            $item = new SelectionItem();
            $item->request = $request;
            $json = Helper::utfDecode($client->get($request)->getBody());

            $json = json_decode($json, true);
            $params = Param::all();
            foreach ($json as &$group) {
                $group['n'] = $params->where('value', $group['n'])->first()->name;
                foreach ($group['d'] as &$selection)
                    $selection['n'] = $params->where('value', $selection['t'])->first()->name;
            }
            $item->json = Helper::utfDecode(json_encode($json));
        }
        $item->save();

        $response = Response::make($item->json, 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    public static function getSubSelections($request)
    {
        $item = SelectionItem::where('request', $request)->first();
        if (!$item) {
            $client = new Client();
            $item = new SelectionItem();
            $item->request = $request;
            $item->json = Helper::utfDecode($client->get($request)->getBody());
        }
        $item->save();

        $response = Response::make($item->json, 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }
}
