<?php

namespace HpdStatApp\Helpers;

use MathPHP\Statistics\Average;
use MathPHP\LinearAlgebra\Matrix;
use MathPHP\LinearAlgebra\MatrixFactory;

class CollocationMethod
{
    /**
     * Contains the forecasting array size.
     *
     * @var int
     */
    private $forecastingSize;
    /**
     * Contains the forecasting array.
     *
     * @var array
     */
    private $forecastingY;
    /**
     * Contains the price array.
     *
     * @var array
     */
    private $y;
    /**
     * Contains the centered price array.
     *
     * @var array
     */
    private $centeredY;
    /**
     * Contains the mean price of price array.
     *
     * @var float
     */
    private $meanY;
    /**
     * Contains the auto covariance array.
     *
     * @var array
     */
    private $s;
    /**
     * Contains the index k.
     *
     * @var int
     */
    private $k;
    /**
     * Contains the index j.
     *
     * @var int
     */
    private $j;
    /**
     * Contains the tau0.
     *
     * @var float
     */
    private $tau0;
    /**
     * Contains the tau1.
     *
     * @var float
     */
    private $tau1;
    /**
     * Contains the rate $tay1 $tau0 less than two of three.
     *
     * @var bool
     */
    private $tau10RateLessThanTwoOfThree;
    /**
     * Contains the coefficient alpha.
     *
     * @var float
     */
    private $alpha;
    /**
     * Contains the coefficient beta.
     *
     * @var float
     */
    private $beta;
    /**
     * Contains the array V.
     *
     * @var array
     */
    private $v;
    /**
     * Contains the matrix A.
     *
     * @var Matrix
     */
    private $matrixA;

    public function calculate()
    {
        $this->centeredY = Math::centeredArray($this->y);
        $this->meanY = Average::mean($this->y);
        $this->s = Math::autoCovarianceArray($this->centeredY);
        $this->j = Math::firstNumberChangePosition($this->s);
        $this->k = Math::firstNumberBiggerThanCovarInHalf($this->s);
        $this->tau1 = $this->k - 1 + ($this->s[$this->k - 1] - $this->s[0] / 2) / ($this->s[$this->k - 1] - $this->s[$this->k]);
        $this->tau0 = $this->j - 1 + $this->s[$this->j - 1] / ($this->s[$this->j - 1] - $this->s[$this->j]);
        $this->tau10RateLessThanTwoOfThree = $this->tau1 / $this->tau0 < 2 / 3;

        $this->alpha = log(2 * cos(M_PI * $this->tau1 / (2 * $this->tau0))) / $this->tau1;
        $this->beta = M_PI / (2 * $this->tau0);

        $n = count($this->y);
        $m = $n + $this->forecastingSize;

        if (true === $this->tau10RateLessThanTwoOfThree) {
            for ($i = 0; $i < $m; ++$i) {
                $this->v[$i] = $this->s[0] * exp(-$this->alpha * $i) * cos($this->beta * $i);
            }
        } else {
            for ($i = 0; $i < $m; ++$i) {
                $this->v[$i] = $this->s[0] * sin($this->alpha * $i) / ($this->alpha * $i);
            }
        }
        $A = [];
        for ($i = 0; $i < $n; ++$i) {
            $A[$i] = [];
            for ($j = 0; $j < $n; ++$j) {
                if ($i > $j) {
                    $A[$i][$j] = 0;
                } else {
                    $A[$i][$j] = $this->v[$j - $i];
                }
            }
        }

        $this->matrixA = new Matrix($A);
        $centeredYMatrix = MatrixFactory::create([$this->centeredY]);
        $centeredYMatrix = $centeredYMatrix->transpose();
        $AYc = $this->matrixA->inverse()->multiply($centeredYMatrix);

        $this->forecastingY = [];
        for ($i = 1; $i <= $this->forecastingSize; ++$i) {
            $subMatrix = new Matrix([array_reverse(array_slice($this->v, $i, $n))]);
            //$subMatrix = $subMatrix->transpose();
            $subMatrix = $subMatrix->multiply($AYc);
            $this->forecastingY[$i] = $subMatrix[0][0] + $this->meanY;
        }
    }

    public function print()
    {
        d($this->y);
        d($this->centeredY);
        d($this->s);
        d($this->k);
        d($this->j);
        d($this->tau1);
        d($this->tau0);
        d($this->tau10RateLessThanTwoOfThree);
        d($this->alpha);
        d($this->beta);
        d($this->v);
        d($this->matrixA->getMatrix());
        d($this->matrixA->inverse()->getMatrix());
        d($this->forecastingY);
    }

    /**
     * @param array $y
     */
    public function setY(array $y)
    {
        $this->y = $y;
    }

    /**
     * @param int $forecastingSize
     */
    public function setForecastingSize(int $forecastingSize)
    {
        $this->forecastingSize = $forecastingSize;
    }

    /**
     * @return array
     */
    public function getForecastingY(): array
    {
        return $this->forecastingY;
    }

    /**
     * @return bool
     */
    public function isTau10RateLessThanTwoOfThree(): bool
    {
        return $this->tau10RateLessThanTwoOfThree;
    }
}
