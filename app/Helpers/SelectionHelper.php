<?php

namespace HpdStatApp\Helpers;

use Carbon\Carbon;
use HpdStatApp\Param;
use GuzzleHttp\Client;
use HpdStatApp\Selection;
use Illuminate\Http\Request;

class SelectionHelper
{
    /**
     * Contains the Selection.
     *
     * @var Selection
     */
    private $selection;
    private $data;
    /**
     * Contains the Client.
     *
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getSelectionData()
    {
        return $this->data;
    }

    public function getSelection()
    {
        return $this->selection;
    }

    public function processData()
    {
        $selectionInfo = [];
        $selectionInfo['info'] = $this->selection->makeHidden(['post_request', 'json_data', 'popular', 'selection_change_at', 'id', 'created_at', 'updated_at'])->toArray();
        $selectionInfo['selection'] = [];

        $jsonDataArray[] = json_decode($this->selection->json_data, true);
        $chart = [];

        foreach ($jsonDataArray[0]['series'][0]['data'] as $value) {
            $this->data['byDate'][$value[3]] = $value[1];
            $chart[] = '['.$value[0].' , '.$value[1].']';
            $selectionInfo['selection'][] = ['date' => $value[3], 'price' => $value[1]];
        }
        $this->data['json'] = Helper::utfDecode(json_encode($selectionInfo, JSON_PRETTY_PRINT));
        $this->data['xml'] = Helper::xmlFromJson($this->data['json']);

        $sum = $countOfDays = 0;
        $lastDate = key($this->data['byDate']);
        $chartByMounth = [];

        Helper::setLocale('ru_UA.utf8');

        foreach ($this->data['byDate'] as $date => $price) {
            if (substr($lastDate, 3, 4) !== substr($date, 3, 4)) {
                if (0 !== $countOfDays) {
                    $this->data['byMounth'][Carbon::parse($lastDate)->formatLocalized('%B %Y')] = $sum / $countOfDays;
                    $this->data['byMounthDmy'][$lastDate] = $sum / $countOfDays;
                    $first = Carbon::parse($lastDate);
                    $first->day = 1;
                    $first->hour = 0;
                    $first->minute = 0;

                    $last = Carbon::parse($lastDate);
                    $last->day = $last->daysInMonth;
                    $last->hour = 23;
                    $last->minute = 59;

                    $chartByMounth[] = '['.$first->timestamp.'000 , '.($sum / $countOfDays).']';
                    $chartByMounth[] = '['.$last->timestamp.'000 , '.($sum / $countOfDays).']';
                }
                $sum = $countOfDays = 0;
            }
            ++$countOfDays;
            $sum += $price;
            $lastDate = $date;
        }

        $this->data['chartByDate'] = '['.implode(' , ', $chart).']';
        $this->data['chartByMounth'] = '['.implode(' , ', $chartByMounth).']';

        $this->data['byDate'] = array_reverse($this->data['byDate']);
        $this->data['byMounth'] = array_reverse($this->data['byMounth']);
    }

    public function findOrCreate(Request $request)
    {
        $postRequest = 'http://domik.ua/nedvizhimost/getStatData.json?build=1&'
        .'statSelectionsRegion='.$request->input('statSelectionsRegion').'&'
        .'statSelectionsType=0&'
        .'statSelectionsSel='.$request->input('statSelectionsSel').'&'
        .'statSelectionsCur='.$request->input('statSelectionsCur').'&'
        .'selections[]=selection2&'
        .'selection2Region='.$request->input('statSelectionsRegion').'&'
        .'selection2Type=0&'
        .'selection2Class='.$request->input('statSelectionsSel').'&'
        .'selection2Id=2&'
        .'selection2priceFld='.$request->input('selection1priceFld').'&'
        .'selection2Approx='.$request->input('selection1Approx')
        .'&selection2statAggVal[]='.urlencode($request->input('selection1statAggVal')[0]);

        $this->selection = Selection::where('post_request', $postRequest)->first();
        if ($this->selection && ! $this->isUpdated()) {
            $this->update();
        } else {
            $this->selection = new Selection();
            $this->setParameters($request);
            $this->setData($postRequest);
        }
        ++$this->selection->popular;
        $this->selection->save();
    }

    public function isUpdated($days = 3)
    {
        $date_change = new Carbon($this->selection->selection_change_at);

        return $date_change->diffInDays(Carbon::now()) < $days;
    }

    public function update()
    {
        $this->selection->json_data = Helper::utfDecode($this->client->post($this->selection->post_request)->getBody());
        $this->selection->selection_change_at = Carbon::now();
    }

    public function setParameters(Request $request)
    {
        $this->selection->region_name = Param::where('value', $request->input('statSelectionsRegion'))->first()->name;
        $this->selection->selection_name = Param::where('value', $request->input('statSelectionsSel'))->first()->name;
        $this->selection->currency_name = Param::where('value', $request->input('statSelectionsCur'))->first()->name;
        $this->selection->parameter_name = Param::where('value', $request->input('selection1priceFld'))->first()->name;
        if (null === Param::where('value', $request->input('selection1Approx'))->first()) {
            $this->selection->approx_name = 'Нет';
        } else {
            $this->selection->approx_name = Param::where('value', $request->input('selection1Approx'))->first()->name;
        }
        $this->selection->subselection_name = $request->input('selection1statAggVal')['0'];
    }

    public function setData($postRequest)
    {
        $this->selection->post_request = $postRequest;
        $this->selection->json_data = Helper::utfDecode($this->client->post($postRequest)->getBody());
        $this->selection->selection_change_at = Carbon::now();
    }
}
