<?php

namespace HpdStatApp\Helpers;

use MathPHP\Statistics\Average;

class Math
{
    public static function subArray($X, $start): array
    {
        return array_slice($X, $start, count($X) + $start);
    }

    public static function firstNumberBiggerThanCovarInHalf(array $X): int
    {
        $covarInHalf = $X[0] / 2;
        $n = count($X);
        for ($i = 0; $i < $n; ++$i) {
            if ($covarInHalf > $X[$i]) {
                return $i;
            }
        }
    }

    public static function firstNumberChangePosition(array $X): int
    {
        $n = count($X);
        for ($i = 0; $i < $n; ++$i) {
            if ($X[$i] < 0) {
                return $i;
            }
        }
    }

    public static function autoCovarianceArray(array $Y): array
    {
        $N = count($Y);
        $n = (int) (count($Y) / 2);
        $meanY = Average::mean($Y);
        $autoCovar = [];
        for ($t = 0; $t < $n; ++$t) {
            $temp = 0;
            for ($i = 0; $i < $N - $t; ++$i) {
                $temp += ($Y[$i] - $meanY) * ($Y[$i + $t] - $meanY);
            }
            $temp /= $N - $t;
            $autoCovar[$t] = $temp;
        }

        return $autoCovar;
    }

    public static function centeredArray(array $X): array
    {
        $mean = Average::mean($X);
        $centered = [];
        foreach ($X as $x) {
            $centered[] = $x - $mean;
        }

        return $centered;
    }
}
