<?php

namespace HpdStatApp\Http\Controllers\API;

use HpdStatApp\Param;
use Illuminate\Http\Request;
use HpdStatApp\Http\Controllers\Controller;


class ForecastingSettingsController extends Controller
{
    public function parametrs($from, $to)
    {
        return Param::whereBetween('id', [$from, $to])->get();
    }
}
