<?php

namespace HpdStatApp\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use HpdStatApp\Helpers\SelectionHelper;
use HpdStatApp\Helpers\CollocationMethod as Collocation;

class ForecastingController extends Controller
{
    /**
     * Contains the Collocation.
     *
     * @var Collocation
     */
    private $collocation;

    /**
     * Contains the SelectionHelper.
     *
     * @var SelectionHelper
     */
    private $selectionHelper;

    public function __construct()
    {
        $this->collocation = new Collocation();
        $this->selectionHelper = new SelectionHelper();
    }

    public function forecasting(Request $request)
    {
        if ($request->isMethod('get')) {
            return View('forecasting');
        }

        $this->selectionHelper->findOrCreate($request);
        $this->selectionHelper->processData();

        $byMounth = $this->selectionHelper->getSelectionData()['byMounthDmy'];

        $price = [];

        $byMounth = array_reverse($byMounth);

        $lastMounth = new Carbon(key($byMounth));
        $lastMounth->addMonth(1);

        $byMounth = array_slice($byMounth, 0, 24); // selection 2 years (24 months)
        foreach ($byMounth as $key => $val) {
            $price[] = $val;
        }

        $countClear = 2;

        $this->collocation->setY($price);
        $this->collocation->setForecastingSize($request->input('forecastingRange') + $countClear);
        $this->collocation->calculate();

        $forecast = $this->collocation->getForecastingY();
        for ($i = 0; $i < $countClear; ++$i) {
            array_shift($forecast);
        } //delet last elemrnt

        $forecastMonth = [];
        $forecastChart = [];
        foreach ($forecast as $key => $val) {
            $forecastMonth[$lastMounth->formatLocalized('%B %Y')] = number_format($val, 2, '.', ' ');

            $first = Carbon::parse($lastMounth);
            $first->day = 1;
            $first->hour = 0;
            $first->minute = 0;

            $last = Carbon::parse($lastMounth);
            $last->day = $last->daysInMonth;
            $last->hour = 23;
            $last->minute = 59;

            $forecastChart[] = '['.$first->timestamp.'000 , '.($val).']';
            $forecastChart[] = '['.$last->timestamp.'000 , '.($val).']';

            $lastMounth->addMonth(1);
        }

        $chartForecast = '['.implode(' , ', $forecastChart).']';
        $forecastMonth = array_reverse($forecastMonth);

        return View('forecasting', ['selection' => $this->selectionHelper->getSelection(), 'data' => $this->selectionHelper->getSelectionData(), 'monthsOfForecasting' => $request->input('forecastingRange'), 'forecast' => $forecastMonth, 'chartForecast' => $chartForecast]);
    }
}
