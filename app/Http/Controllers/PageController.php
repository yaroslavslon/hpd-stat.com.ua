<?php

namespace HpdStatApp\Http\Controllers;

use HpdStatApp\User;
use HpdStatApp\Selection;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        return redirect()->route('forecasting');
    }

    public function records()
    {
        $user = Auth::user();
        if ('admin' !== $user->role) {
            return View('deniedAccess');
        }

        $selections = Selection::whereNotNull('json_info')->paginate(5);

        return View('records')->with('selections', $selections);
    }

    public function users()
    {
        $user = Auth::user();
        if ('admin' !== $user->role) {
            return View('deniedAccess');
        }

        $users = User::paginate(5);

        return View('users', compact('users'));
    }

    public function login()
    {
        return View('login');
    }

    public function register()
    {
        return View('register');
    }
}
