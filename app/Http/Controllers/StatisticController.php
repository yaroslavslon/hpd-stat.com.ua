<?php

namespace HpdStatApp\Http\Controllers;

use Illuminate\Http\Request;
use HpdStatApp\Helpers\SelectionHelper;
use HpdStatApp\Helpers\SelectionItemHelper;
use Symfony\Component\VarDumper\Cloner\Data;

class StatisticController extends Controller
{
    /**
     * Contains the SelectionHelper.
     *
     * @var SelectionHelper
     */
    private $selectionHelper;

    public function __construct()
    {
        $this->selectionHelper = new SelectionHelper();
    }

    public function getSelections($r)
    {
        return SelectionItemHelper::getSelections('http://domik.ua/nedvizhimost/getStatData.json?getSelections=1&r='.$r.'&t=0');
    }

    public function getSelectionItems($r, $aggValues, $cur)
    {
        return SelectionItemHelper::getSubSelections('http://domik.ua/nedvizhimost/getStatData.json?r='.$r.'&t=0&getAggValues='.$aggValues.'&cur='.$cur);
    }

    public function getStatistic(Request $request)
    {
        if ($request->isMethod('get')) {
            return View('statistic');
        }
        $this->selectionHelper->findOrCreate($request);
        $this->selectionHelper->processData();

        return View('statistic', ['selection' => $this->selectionHelper->getSelection(), 'data' => $this->selectionHelper->getSelectionData()]);
    }
}
