<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('home');
Route::get('admin/records', 'PageController@records')->name('records')->middleware('auth');
Route::get('admin/users', 'PageController@users')->name('users')->middleware('auth');

Route::get('getSelections/{r}', 'StatisticController@getSelections')->name('api.selections');

Route::get('getSelectionItems/{r}/{aggValues}/{cur}', 'StatisticController@getSelectionItems')->name('api.selection_items');

Route::match(['get', 'post'], 'statistic', 'StatisticController@getStatistic')->name('statistic');
Route::match(['get', 'post'], 'forecasting', 'ForecastingController@forecasting')->name('forecasting');

Auth::routes();

Route::prefix('api')->group(function () {
    Route::get('params/from/{from}/to/{to}', 'API\ForecastingSettingsController@parametrs')->name('api.params.between');
});
